import 'dart:convert';
import 'dart:io';
import 'dart:math';

void main(List<String> arguments) async {
  dynamic jsonData = await readJsonFile('bin/Pizza.json');
  List<String> Menu = [];
  Menu.addAll(jsonData.keys);
  List<String> choice = ["cheese", "sausage", "pepperoni"];

  int level = 1;
  int score = 0;
  int custormer = 1;
  //welcome
  print("Welcome to Guess Pizza");
  print("------------------------------------");
  stdout.write(">Start");
  stdin.readLineSync(encoding: utf8);
  //play
  while (true) {
    print("------------------------------------");
    stdout.write(">Level:$level\n");
    print("------------------------------------");
    stdout.write("Get order\n");
    stdout.write("Customer: Can i have\n");
    var MenuR = randomMenu(Menu);
    stdout.write("...\n");
    stdout.write(">Ok");
    stdin.readLineSync(encoding: utf8);
    print("------------------------------------");
    stdout.write("Level: $level\n");
    stdout.write("Score: $score\n");
    stdout.write("Custormer: $custormer/5\n");
    print("------------------------------------");
    var answer = jsonData[MenuR()];
    print("Quiz: $answer");
    print("Choice: $choice");
    var response = stdin.readLineSync(encoding: utf8);
    var check = answer.contains(response);
    if (check == true) {
      score = score + 10;
      custormer = custormer + 1;
      if ((custormer - 1) == 5) {
        level = level + 1;
        custormer = 1;
      }
    } else {
      print("You lose");
      print("***");
      print("Score: $score");
      break;
    }
  }
}

dynamic readJsonFile(String filePath) async {
  var input = await File(filePath).readAsString();
  var map = jsonDecode(input);
  return map;
}

String Function() randomMenu(List Manu) {
  return () {
    final _random = new Random();
    return Manu[_random.nextInt(Manu.length)];
  };
}
